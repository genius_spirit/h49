let createChessBoard = (value) => {
  let board = "";
  for (let i = 1; i < value*value; i++) {
    if ((i % (value + 1)) === 0) {
      board += "\n";
    } else if (i % 2 !== 0) {
      board += " £ ";
    } else {
      board += " $ ";
    }
  }

  return board;
};

console.log(createChessBoard(8));